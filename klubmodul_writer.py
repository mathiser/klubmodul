from datetime import datetime, timedelta, time
from time import sleep

import bs4
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException, \
    ElementClickInterceptedException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys


class KlubModul:
    def __init__(self, settings, driver):
        self.settings = settings

        self.driver = driver
        self.start_datetime = datetime.strptime(self.settings["start_date"] + "-{hours}-{minutes}".format(
            hours=self.settings["start_hour"], minutes=self.settings["start_minute"]), '%d-%m-%Y-%H-%M')
        self.end_datetime = datetime.strptime(self.settings["end_date"] + "-{hours}-{minutes}".format(
            hours=self.settings["end_hour"], minutes=self.settings["end_minute"]), '%d-%m-%Y-%H-%M')

        self.booking_url = self.settings["booking_url"]
        self.events = []
        self.existing_events = []
        # self.get_existing_events()
        # self.generate_events()
        self.create_events_online()


    def generate_events(self):
        self.events = []
        event_start_datetime = self.start_datetime
        #tjekker datointerval
        while event_start_datetime <= self.end_datetime:
            #tjekker event inden for åbningstider ellers næste dag
            if event_start_datetime.time() > time(hour=self.settings["end_hour"], minute=self.settings["end_minute"]):
                nextdate = event_start_datetime + timedelta(days=1)
                event_start_datetime = datetime(day=nextdate.day, month=nextdate.month,
                                                year=nextdate.year, hour=self.settings["start_hour"],
                                                minute=self.settings["start_minute"])

            #Hvornår stopper eventet
            event_end_datetime = event_start_datetime + timedelta(hours=self.settings["duration_hours"],
                                                                  minutes=self.settings["duration_minutes"])
            # Event to create
            event = {
                "start_datetime": event_start_datetime,
                "end_datetime": event_end_datetime,
                "headcount": self.headcount_junior_time(event_start_datetime),
                "max_waiting_list": self.settings["max_waiting_list"],
                "area_number": self.settings["area_number"]
            }

            #Tjek om eventet allerede er oprettet
            if not self.event_exists(event):
                self.events.append(event)

            #increase event_start_datetime og loop igen
            event_start_datetime += timedelta(minutes=self.settings["arrival_interval_minutes"])

    def event_exists(self, event):### tilføj tjek for område

        temp_dict = {
            "area": self.settings["areas"][event["area_number"]],
            "datetime": event["start_datetime"]
        }

        if temp_dict in self.existing_events:
            print("event already created")
            return True
        else:
            return False

    def headcount_junior_time(self, d):
        if d.weekday() == 0:
            if time(17, 0) <= d.time() < time(19, 0):
                return 0
        if d.weekday() == 2:
            if time(18, 0) <= d.time() < time(20, 0):
                return 0
        # if d.weekday() == 3:
        #     if time(16, 30) <= d.time() <= time(17, 0):
        #         return 0
        return self.settings['headcount']

    def new_booking(self):
        self.new_window()
        self.driver.get(self.booking_url)

        while True:
            try:
                self.driver.find_element_by_id("30sbook")
                self.driver.execute_script("document.getElementById('30sbook').style.display = 'none';")
                break
            except Exception as e:
                print(e)
                sleep(1)

    def create_event(self, event):
        self.new_booking()
        if self.event_exists(event):
            print("Event: ", str(event["start_datetime"]), " exists")
            self.close_window()
            return
        else:
            print("Event: ", str(event["start_datetime"]), " does not exist -- Creating")

        #Overskrift
        self.driver.find_element_by_id("ctl00_ContentPlaceHolderBody_txtName").send_keys(self.settings["areas"][self.settings["area_number"]])

        #Område
        area = self.driver.find_element_by_id("ctl00_ContentPlaceHolderBody_ddPool_chosen")
        area.click()
        self.wait_by_element_class_name(area, "chosen-results")
        dropdown = area.find_element_by_class_name("chosen-results")
        areas = dropdown.find_elements_by_tag_name("li")
        areas[self.settings["area_number"]].click()

        #Dato
        date = self.driver.find_element_by_id("ctl00_ContentPlaceHolderBody_txtStartDate")
        date.clear()
        date_str = event["start_datetime"].strftime("%d-%m-%Y")
        date.send_keys(date_str)
        date.send_keys(Keys.ENTER)


        #start hours
        self.set_time("ctl00_ContentPlaceHolderBody_ddStartHour_chosen", event["start_datetime"].hour)

        #start_minutes
        self.set_time("ctl00_ContentPlaceHolderBody_ddStartMinute_chosen", event["start_datetime"].minute)

        # end_hours
        self.set_time("ctl00_ContentPlaceHolderBody_ddEndHour_chosen", event["end_datetime"].hour)

        # end_minutes
        self.set_time("ctl00_ContentPlaceHolderBody_ddEndMinute_chosen", event["end_datetime"].minute)

        # max participants
        self.driver.find_element_by_id("ctl00_ContentPlaceHolderBody_txtMaxNumberEnrollment").send_keys(event["headcount"])

        # max waiting list
        self.driver.find_element_by_id("ctl00_ContentPlaceHolderBody_txtMaxNumberWaitinglist").send_keys(event["max_waiting_list"])

        #instructor
        ins = self.driver.find_element_by_id("ctl00_ContentPlaceHolderBody_ddInstructor_chosen")
        ins.click()
        self.wait_by_element_class_name(ins, "chosen-results")
        ins.find_element_by_class_name("chosen-results").click()

        #send + ref for testing if send properly
        ref = self.driver.find_element_by_id("bookingMaintenanceOverview").find_element_by_tag_name("tbody").find_elements_by_tag_name("tr")
        self.driver.execute_script('WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions('
                                   '"ctl00$ContentPlaceHolderBody$btnSave", "", true, "valGroupTeam", "", false, '
                                   'true))')

        #sleep until send
        counter = 0
        while counter < 120:
            sleep(1)
            counter += 1
            try:
                self.update_existing_events()
                if self.event_exists(event):
                    break
            except:
                pass
        else:
            raise Exception

        print("Created with: ", str(event["start_datetime"]))
        self.close_window()


    def new_window(self):
        ActionChains(self.driver).key_down(Keys.CONTROL).key_down("n").key_up("n").key_up(Keys.CONTROL)
        #self.driver.execute_script("window.open()")
        self.driver.switch_to.window(self.driver.window_handles[-1])
        return self.driver.window_handles[-1]

    def close_window(self):
        ActionChains(self.driver).key_down(Keys.CONTROL).key_down("w").key_up(Keys.CONTROL).key_up("w")
        #self.driver.execute_script("window.close()")
        self.driver.switch_to.window(self.driver.window_handles[0])
        return self.driver.window_handles[0]

    def set_time(self, element_id, value):
        while True:
            try:
                h = self.driver.find_element_by_id(element_id)
                h.click()
                hin = h.find_element_by_tag_name("input")
                hin.send_keys(str(value))
                hin.send_keys(Keys.ENTER)
                break
            except (NoSuchElementException, StaleElementReferenceException, ElementClickInterceptedException):
                sleep(1)

    def wait_by_element_id(self, parent, element_id):
        while True:
            try:
                el = parent.find_element_by_id(element_id)
                return el
            except (NoSuchElementException, StaleElementReferenceException, ElementClickInterceptedException):
                sleep(0.1)

    def wait_by_element_class_name(self, parent, element_class_name):
        while True:
            try:
                el = parent.find_element_by_class_name(element_class_name)
                return el
            except (NoSuchElementException, StaleElementReferenceException, ElementClickInterceptedException):
                sleep(0.1)

    def create_events_online(self):
        self.get_existing_events()
        self.generate_events()
        for ev in self.events:
            while True:
                try:
                    self.create_event(ev)
                    break
                except Exception as e:
                    print(e)
                    self.get_existing_events()
                    self.generate_events()
                    self.close_window()


    def get_existing_events(self):
        self.new_booking()
        self.update_existing_events()
        self.close_window()


    def update_existing_events(self):
        self.existing_events = []

        soup = bs4.BeautifulSoup(self.driver.page_source, 'lxml')
        table = soup.find("table", {"id": "bookingMaintenanceOverview"})
        thead = table.thead
        headers = [n.text for n in thead.find("tr").find_all("td")]

        tbody = table.tbody
        trs = tbody.find_all("tr")
        temp_events =[]
        for tr in trs:
            row_dict = {}
            tds = tr.find_all("td")
            for i in range(len(tds)):
                span = tds[i].find("span")
                if span is not None:
                    row_dict[headers[i]] = span.text

            temp_events.append(row_dict)

        for e in temp_events:
            d, m, y = e["Dato"].split('.')
            h, min = e['Fra kl.'].split(":")
            tempdate = datetime(year=int(y), month=int(m), day=int(d), hour=int(h), minute=int(min))
            foo = {"area": e['Sted'],
                   "datetime": tempdate}
            self.existing_events.append(foo)






